package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"os"

	"github.com/gocarina/gocsv"
	"github.com/jfyne/csvd"
)

func ReadCSV(filename string) ([]*Result, error) {

	file, err := os.OpenFile(filename, os.O_RDONLY, os.ModePerm)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	gocsv.SetCSVReader(func(in io.Reader) gocsv.CSVReader {
		r := csvd.NewReader(in)
		r.LazyQuotes = true
		return r
	})

	results := []*Result{}

	if err = gocsv.UnmarshalFile(file, &results); err != nil {
		return nil, err
	}

	return results, nil
}

func WriteCSV(filename string, results []*Result) error {

	gocsv.SetCSVWriter(func(out io.Writer) *gocsv.SafeCSVWriter {
		writer := csv.NewWriter(out)
		writer.Comma = ';'
		return gocsv.NewSafeCSVWriter(writer)
	})

	csvContent, err := gocsv.MarshalBytes(results)
	if err != nil {
		return err
	}

	fmt.Printf("%sCreated %v with test results%s\n", InfoColor, filename, ResetColor)

	return os.WriteFile(filename, csvContent, os.ModePerm)
	// return nil
}

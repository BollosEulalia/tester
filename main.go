package main

import (
	"fmt"
	"os"

	"github.com/alexflint/go-arg"
)

const (
	ErrorColor  = "\033[31m" //red
	InfoColor   = "\033[1;36m"
	ResetColor  = "\033[0m" //default
	NoticeColor = "\033[1;34m"
)

func osExit(err error) {
	fmt.Fprintf(os.Stderr, "%s%v%s\n", ErrorColor, err.Error(), ResetColor)
	os.Exit(1)
}

func main() {

	var args CLIArgs
	arg.MustParse(&args)

	cfg, err := NewConfig(args.Config)
	if err != nil {
		osExit(err)
	}
	if err = args.CheckArgs(cfg); err != nil {
		osExit(err)
	}

	assignment, ok := cfg.Testing.Assignments[args.Assignment]
	if !ok {
		osExit(fmt.Errorf("assignment '%v' not configured. Please add it to the configuration.\n"+
			"Configured assignments: %v", args.Assignment, cfg.Testing.Assignments))
	}

	var results []*Result

	if len(args.Csv) > 0 {

		results, err = ReadCSV(args.Csv)
		if err != nil {
			osExit(err)
		}
	}

	fmt.Println("Running with ", args.Threads, " threads")

	tester := cfg.Testing.Tester
	tester.Init(&args, results, assignment)

	tester.CheckFilesModified()

	if !args.Read && tester.ConfigOk() {
		if err = tester.RunTests(); err != nil {
			osExit(err)
		}
	}

	tester.ReadAllResultJson()

	WriteCSV(args.Output, results)

	if args.Clean && len(args.Results) > 0 {
		for directory := range tester.results {
			tester.DeleteFiles(tester.filesToCopy, directory)
			tester.DeleteFiles(tester.filesToSymlink, directory)
		}
	}

}

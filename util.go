package main

import (
	"fmt"
	"strings"
)

func yesNo(prompt string) bool {

	var in string
	fmt.Println(prompt)
	fmt.Println("Select[Yes/No]")
	fmt.Scanln(&in)

	return strings.Contains(strings.ToLower(in), "y")
}

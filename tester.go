package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"sync"

	"github.com/alitto/pond"
	glob "github.com/ganbarodigital/go_glob"
)

type Tester struct {
	assignment                  *Assignment
	Command                     string
	Arguments                   string
	ResultJsonFilename          string
	filesToCopy                 []string
	filesToSymlink              []string
	ignoredFiles                []string
	filesNotAllowedToBeModified []string
	publicTestcases             int
	privateTestcases            int
	solutionDir                 string
	resultsDirs                 []string
	results                     map[string][]*Result
	overwrite                   bool

	filesToCopyAbsPath    []string
	filesToSymlinkAbsPath []string
	silent                bool
	threads               int
	mutex                 sync.Mutex
	testcaseCount         int
}

func (tester *Tester) Init(args *CLIArgs, results []*Result, assignment *Assignment) error {

	tester.solutionDir = args.Solution
	tester.resultsDirs = args.Results
	tester.ResultJsonFilename = args.Json
	tester.overwrite = args.Overwrite
	tester.silent = args.Silent
	tester.threads = args.Threads
	tester.results = make(map[string][]*Result)

	tester.assignment = assignment

	tester.ignoredFiles = assignment.IgnoredFiles
	tester.publicTestcases = assignment.PublicTestcases
	tester.privateTestcases = assignment.PrivateTestcases
	tester.testcaseCount = assignment.PublicTestcases + assignment.PrivateTestcases
	tester.filesNotAllowedToBeModified = assignment.FilesNotAllowedToBeModified

	for i := range args.Results {
		for j := range results {

			if file, err := os.Stat(args.Results[i]); err == nil && file.IsDir() &&
				len(results[j].DirectoryName) > 0 && strings.Contains(args.Results[i], results[j].DirectoryName) {

				results[j].PassedPrivate = 0
				results[j].PassedPublic = 0
				results[j].PassedTotal = 0
				results[j].TotalTestcases = int64(tester.testcaseCount)
				tester.results[args.Results[i]] = append(tester.results[args.Results[i]], results[j])
				break
			}
		}
	}

	if len(tester.results) == 0 {
		for i := range args.Results {
			if fsinfo, err := os.Stat(args.Results[i]); err == nil && fsinfo.IsDir() {
				tester.results[args.Results[i]] = []*Result{
					&Result{DirectoryName: fsinfo.Name(), TotalTestcases: int64(tester.testcaseCount)}}
			}
		}
	}
	fmt.Println(len(tester.results))

	filesToCopy := make([]string, 0, len(assignment.FilesToCopy))
	filesToSymlink := make([]string, 0, len(assignment.FilesToSymlink))

	for _, filename := range assignment.FilesToCopy {
		if _, err := os.Stat(filepath.Join(tester.solutionDir, filename)); err == nil {
			filesToCopy = append(filesToCopy, filename)
		}
	}
	for _, filename := range assignment.FilesToSymlink {
		if _, err := os.Stat(filepath.Join(tester.solutionDir, filename)); err == nil {
			filesToSymlink = append(filesToSymlink, filename)
		}
	}

	tester.filesToCopy = filesToCopy
	tester.filesToSymlink = filesToSymlink

	tester.CreateAbsolutePaths()

	return nil
}

func (tester *Tester) CreateAbsolutePaths() error {
	var err error
	tester.filesToCopyAbsPath = make([]string, len(tester.filesToCopy))
	tester.filesToSymlinkAbsPath = make([]string, len(tester.filesToSymlink))
	for i := range tester.filesToCopy {
		tester.filesToCopyAbsPath[i], err = filepath.Abs(filepath.Join(tester.solutionDir, tester.filesToCopy[i]))
		if err != nil {
			return err
		}
	}
	for i := range tester.filesToSymlink {
		tester.filesToSymlinkAbsPath[i], err = filepath.Abs(filepath.Join(tester.solutionDir, tester.filesToSymlink[i]))
		if err != nil {
			return err
		}
	}
	return nil
}

func (tester *Tester) MatchIgnoredGlob(filename string) bool {
	for i := range tester.ignoredFiles {
		if matched, _ := glob.NewGlob(tester.ignoredFiles[i]).Match(filename); matched {
			return true
		}

	}
	return false
}

func (tester *Tester) MatchCopyGlob(filename string) bool {

	if tester.MatchIgnoredGlob(filename) {
		return false
	}

	for i := range tester.filesToCopy {
		if matched, _ := glob.NewGlob(tester.filesToCopy[i]).Match(filename); matched {
			return true
		}
	}

	return false
}

func (tester *Tester) MatchSymlinkGlob(filename string) bool {

	if tester.MatchIgnoredGlob(filename) {
		return false
	}

	for i := range tester.filesToSymlink {
		if matched, _ := glob.NewGlob(tester.filesToSymlink[i]).Match(filename); matched {
			return true
		}
	}
	return false
}

func (tester *Tester) ConfigOk() bool {
	directories := make([]string, 0, len(tester.results))
	for key := range tester.results {
		directories = append(directories, key)
	}
	for i := range directories {
		println(directories[i])
	}

	return yesNo(fmt.Sprintf("\n%s%v from %v will be copied to the directories listed above.\n"+
		"%v will be symlinked.\n%sThe command to be executed is '%v %v'%s\n"+
		"The name of the testresult.json is %v\nIs this correct?",
		InfoColor, tester.filesToCopy, tester.solutionDir,
		tester.filesToSymlink, NoticeColor, tester.Command, tester.Arguments, ResetColor, tester.ResultJsonFilename))

}

func (tester *Tester) CheckFilesModified() {

	for directory := range tester.results {
		modified := make([]string, 0, len(tester.filesNotAllowedToBeModified))
		for _, filename := range tester.filesNotAllowedToBeModified {
			if content, err := ioutil.ReadFile(filepath.Join(directory, filename)); err == nil {
				if solutionContent, err := ioutil.ReadFile(filepath.Join(tester.solutionDir, filename)); err == nil {
					if bytes.Compare(content, solutionContent) > 0 {
						modified = append(modified, filename)
					}
				}
			}
		}
		for i := range tester.results[directory] {
			tester.results[directory][i].UnallowedModified = strings.Join(modified, ",")
		}
	}

}

func (tester *Tester) RunTests() error {

	fmt.Println("running tests")
	// var err error

	pool := pond.New(tester.threads, len(tester.results))

	done := 0

	for directory := range tester.results {
		dir := directory
		pool.Submit(func() {
			tester.CopyFiles(dir)
			tester.SymlinkFiles(dir)
			tester.Execute(dir)
			tester.mutex.Lock()
			if tester.silent {
				done++
				fmt.Printf("\rTests done: %0*d/%d", len(fmt.Sprint(len(tester.results))), done, len(tester.results))
			}
			tester.mutex.Unlock()
		})

	}

	println("")

	pool.StopAndWait()

	return nil
}

func (tester *Tester) RunCommand(cmd *exec.Cmd, message string) error {
	if !tester.silent {
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
	}
	return cmd.Run()
}

func (tester *Tester) Execute(directory string) error {
	cmd := exec.Command(tester.Command, strings.Split(tester.Arguments, " ")...)
	cmd.Dir = directory

	return tester.RunCommand(cmd, fmt.Sprintln(tester.Command, tester.Arguments, " in ", directory))
}

func (tester *Tester) DeleteFiles(toDelete []string, directory string) error {

	toDelete2 := make([]string, len(toDelete))

	for i := range toDelete {
		toDelete2[i] = filepath.Join(directory, toDelete[i])
	}

	cmd := exec.Command("rm", "-rf")
	cmd.Args = append(cmd.Args, toDelete2...)

	return tester.RunCommand(cmd, fmt.Sprintln("rm -rf", toDelete2))

}

func (tester *Tester) CopyFiles(outDir string) error {

	args := make([]string, 0, len(tester.filesToCopyAbsPath)+2)
	args = append(args, "--recursive")
	if tester.overwrite {
		args = append(args, "--force")
		tester.DeleteFiles(tester.filesToCopy, outDir)
	}
	var err error
	for i := range tester.filesToCopy {
		args2 := append(args, tester.filesToCopyAbsPath[i], tester.filesToCopy[i])
		cmd := exec.Command("cp", args2...)
		cmd.Dir = outDir
		err = tester.RunCommand(cmd, fmt.Sprintln("cp", strings.Join(args2, " ")))
		// args2 = append(args2)
	}
	// args = append(args, tester.filesToCopyAbsPath...)
	// args = append(args, outDir)

	// cmd := exec.Command("cp", args...)
	// return tester.RunCommand(cmd, fmt.Sprintln("cp", strings.Join(args, " ")))
	return err
}

func (tester *Tester) SymlinkFiles(outDir string) error {

	args := make([]string, 0, len(tester.filesToSymlinkAbsPath)+2)
	args = append(args, "-s")
	if tester.overwrite {
		args = append(args, "--force")
		tester.DeleteFiles(tester.filesToSymlink, outDir)
	}
	// args = append(args, tester.filesToSymlinkAbsPath...)
	// args = append(args, outDir)

	// cmd := exec.Command("ln", args...)
	// // cmd.Dir = outDir
	// return tester.RunCommand(cmd, fmt.Sprintln("ln", strings.Join(args, " ")))
	var err error
	for i := range tester.filesToSymlink {
		args2 := append(args, tester.filesToSymlinkAbsPath[i], tester.filesToSymlink[i])
		cmd := exec.Command("ln", args2...)
		cmd.Dir = outDir
		err = tester.RunCommand(cmd, fmt.Sprintln("ln", strings.Join(args2, " ")))
		// args2 = append(args2)
	}

	return err

}

func (tester *Tester) ReadAllResultJson() {

	println("reading results")

	for directory, results := range tester.results {
		tester.ParseResultJson(filepath.Join(directory, tester.ResultJsonFilename), results)
	}

}

func (tester *Tester) ParseResultJson(filename string, output []*Result) error {

	file, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}

	testresult := ResultJson{}
	if err = json.Unmarshal(file, &testresult); err != nil {
		return err
	}

	for i := range output {

		// if !testresult.Binary.Compiled {
		// 	output[i].Compiled = false
		// 	continue//return nil
		// }
		output[i].Compiled = testresult.Binary.Compiled

		for _, tc := range testresult.Testcases {

			if tc.Passed {
				if tc.Protected {
					output[i].PassedPrivate++
				} else {
					output[i].PassedPublic++
				}
				output[i].MemoryErrors += tc.MemErrors
				output[i].MemoryLeaks += tc.MemLeaks
			} else {
				output[i].MemoryErrorsFailed += tc.MemErrors
				output[i].MemoryLeaksFailed += tc.MemLeaks
				if tc.Timeout {
					output[i].Timeouts++
				}
			}
		}

		for _, warningCount := range testresult.Binary.Warnings {
			output[i].Warnings += warningCount
		}

		output[i].PassedTotal = output[i].PassedPublic + output[i].PassedPrivate

		if output[i].TotalTestcases > 0 {
			output[i].PercentagePassed = float64(output[i].PassedTotal) / float64(output[i].TotalTestcases) * 100.0
			output[i].PercentagePublic = float64(output[i].PassedPublic) / float64(tester.publicTestcases) * 100.0
			output[i].PercentagePrivate = float64(output[i].PassedPrivate) / float64(tester.privateTestcases) * 100.0
		}
	}

	return nil
}

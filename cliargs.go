package main

import (
	"fmt"
	"math"
	"runtime"
)

type CLIArgs struct {
	Assignment string   `arg:"required, -a, --assignment" help:"Assignment to test"`
	Solution   string   `arg:"-s, --solution" help:"Directory containing the solution"`
	Results    []string `arg:"-r, --results"`
	Threads    int      `arg:"-t, --threads"`
	Clean      bool     `help:"remove copied and linked files after testing"`
	Config     string   `arg:"-c, --config" default:"tester.toml"`
	Csv        string   `help:"results.csv created by repomanager"`
	Overwrite  bool     `help:"Overwrite files from submissions with files from solution"`
	Output     string   `arg:"-o, --output" default:"testlog.csv" help:"name of the output .csv"`
	Json       string   `arg:"-j, --json" help:"result.json from testrunner"`
	Read       bool     `arg:"-r, --read" help:"only read and parse testreport.json files and create testlot.csv"`
	Silent     bool     `help:"suppress output"`
}

func (args *CLIArgs) CheckArgs(cfg *Config) error {

	if args.Threads < 1 || args.Threads > runtime.NumCPU() {
		args.Threads = int(math.Max(1, float64(runtime.NumCPU())/2.0))
	}

	if len(args.Json) < 1 {
		args.Json = cfg.Testing.Tester.ResultJsonFilename
	}
	if len(args.Json) < 1 {
		args.Json = "testreport.json"
	}

	if !args.Read && (len(args.Solution) == 0 || len(args.Results) == 0) {
		return fmt.Errorf("you need to provide the path to solution and the paths to the results")
	}

	return nil
}

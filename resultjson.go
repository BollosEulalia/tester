package main

// type MinTestcase struct {
// 	Name       string
// 	Passed     bool
// 	Timeout    bool
// 	MemErrors  int64
// 	MemLeaks   int64
// 	Statuscode int
// 	Protected  bool
// }
type Testcase struct {
	// AddDistance float64 `json:"add_distance"`
	// Distance    float64
	// Kind        string
	// Implemented bool
	MemErrors int64 `json:"mem_errors"`
	MemLeaks  int64 `json:"mem_leaks"`
	Name      string
	Passed    bool
	Protected bool
	// Result      string
	Statuscode int
	Timeout    bool
}

type Binary struct {
	Compiled bool
	Errors   *int64
	Warnings map[string]int64
}

type ResultJson struct {
	Testcases     []Testcase
	Binary        Binary
	passedPublic  int64
	passedPrivate int64
}

func (r *ResultJson) ParseTestcases() {
	for _, tc := range r.Testcases {
		if tc.Passed {
			if tc.Protected {
				r.passedPrivate++
			} else {
				r.passedPublic++
			}
		}
	}
}

package main

import (
	"errors"
	"os"

	"github.com/BurntSushi/toml"
)

type Assignment struct {
	FilesNotAllowedToBeModified []string
	FilesToCopy                 []string
	FilesToSymlink              []string
	IgnoredFiles                []string
	PublicTestcases             int
	PrivateTestcases            int
}
type Test struct {
	Tester      *Tester
	Assignments map[string]*Assignment
}

type Config struct {
	// Testing *Tester
	Testing *Test // Assignments map[string]*Tester
}

func NewConfig(filename string) (*Config, error) {
	cfg := &Config{}

	if _, err := os.Stat(filename); os.IsNotExist(err) || err != nil {
		return nil, err
	}

	if _, err := toml.DecodeFile(filename, cfg); err != nil {
		return nil, err
	}
	if cfg.Testing == nil {
		return nil, errors.New("config does not contain section [Testing]")
	}

	if cfg.Testing == nil {
		cfg.Testing = &Test{}
		cfg.Testing.Assignments = make(map[string]*Assignment)
	}

	return cfg, nil
}

func (cfg *Config) Write(filename string) error {
	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer file.Close()
	encoder := toml.NewEncoder(file)
	return encoder.Encode(cfg)

}

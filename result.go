package main

type Result struct {
	Student            string
	MatriculationNr    string
	Email              string
	ProjectName        string `csv:"Repository Name"`
	DirectoryName      string `csv:"Directory Name"`
	Members            string `csv:"Members(Repolist)"`
	Branch             string //`csv:"Branch"`
	CommitDate         string `csv:"CommitDate"`
	UnallowedModified  string
	Compiled           bool
	Warnings           int64
	Errors             string
	Timeouts           int64
	TotalTestcases     int64
	PercentagePassed   float64
	PercentagePublic   float64
	PercentagePrivate  float64
	PassedTotal        int64  `csv:"Passed Total"`
	PassedPublic       int64  `csv:"Passed Public"`
	PassedPrivate      int64  `csv:"Passed Private"`
	MemoryErrors       int64  `csv:"MemErrors(Passed)"`
	MemoryLeaks        int64  `csv:"MemLeaks(Passed)"`
	MemoryErrorsFailed int64  `csv:"MemErrors(Failed)"`
	MemoryLeaksFailed  int64  `csv:"MemLeaks(Failed)"`
	HttpURL            string `csv:"HTTP-URL"`
	SshURL             string `csv:"SSH-URL"`
	// Testcases          []Testcase
}

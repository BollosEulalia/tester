module tester

go 1.18

require (
	github.com/BurntSushi/toml v1.1.0 // indirect
	github.com/alexflint/go-arg v1.4.3 // indirect
	github.com/alexflint/go-scalar v1.1.0 // indirect
	github.com/alitto/pond v1.7.2 // indirect
	github.com/chzyer/readline v0.0.0-20180603132655-2972be24d48e // indirect
	github.com/ganbarodigital/go_glob v1.0.0 // indirect
	github.com/gocarina/gocsv v0.0.0-20220422102445-f48ffd81e276 // indirect
	github.com/jfyne/csvd v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20181122145206-62eef0e2fa9b // indirect
)
